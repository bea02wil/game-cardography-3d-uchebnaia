﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMover : MonoBehaviour
{     
    Vector3 pos;
    public float offsetForTweened;

    private void Start()
    {
        ViewsToggler.OnViewMode += ChangeViewMode;      
        pos = transform.localPosition;        
    }

    public void ChangeViewMode(WhichButton someWhichButton)
    {
        if (someWhichButton == WhichButton.Rink)
        {
            TweenPosition.Begin(gameObject, 0.3f, new Vector3(pos.x + offsetForTweened, pos.y, pos.z));
        }
        else if (someWhichButton == WhichButton.CharDes)
        {
            TweenPosition.Begin(gameObject, 0.3f, pos);
        }
    }   
}
