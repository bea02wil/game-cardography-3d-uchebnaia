﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WhichSuit
{
    Steampunk,
    Blues,
    Santa,
    Astronaut
};

public class CustumeButton : MonoBehaviour
{
    public WhichSuit mWichSuit;   
    public delegate void SuitHandler(WhichSuit someWhichSuit);  
    public static event SuitHandler OnSuit;
    
    private void Start()
    {           
        OnSuit?.Invoke(WhichSuit.Steampunk);
    }
  
    public void OnClick() //не нашел эту функцию в NGUI, пришлось делать паблик и давать в ивет OnClick класса Button в инспекторе
    {    
        OnSuit?.Invoke(mWichSuit);
    }

}
