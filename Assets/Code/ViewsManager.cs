﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewsManager : MonoBehaviour
{
    public Camera mainCam;
    public Camera charDesignCam;
    public Transform hero;

    private void Start()
    {
        ViewsToggler.OnViewMode += ChangeViewMode;
    }

    private void ChangeViewMode(WhichButton mWhichButton)
    {
        bool isDesigner = false;
        isDesigner = mWhichButton == WhichButton.CharDes ? true : false;

        charDesignCam.enabled = isDesigner;
        mainCam.enabled = !isDesigner;
    }
}
