﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RinkJumper : MonoBehaviour
{
    Vector3 targetPosition;
    public Transform hero;
    public float jumpSpeed;
    private void OnClick() //это метод NGUI
    {
        Vector3 hitPoint = UICamera.lastHit.point;      
        targetPosition = new Vector3(hitPoint.x, hero.position.y, hitPoint.z);

        //HeroMover heroMover = hero.GetComponent<HeroMover>();
        //if (heroMover.heroAnimator)
        //    heroMover.heroAnimator.SetBool("jumping", true);

        Animator heroAnimator = hero.GetComponent<Animator>();
        if (heroAnimator)
            heroAnimator.SetBool("jumping", true);

        StartCoroutine(Jumper());
    }

    private IEnumerator Jumper()
    {
        Vector3 heroInitPos = hero.position;
        float i = 0;

        StartCoroutine(hero.GetComponent<HeroMover>().TimelyTurner(hero.localPosition));

        while (i < 1)
        {
            hero.position = Vector3.Lerp(heroInitPos, targetPosition, i); //Lerp () - перемесит персонажа с одних координат вектора до второго
            i += Time.deltaTime * jumpSpeed;
            yield return null;
        }

        Animator heroAnimator = hero.GetComponent<Animator>();
        if (heroAnimator)
            heroAnimator.SetBool("jumping", false);
    }
}
