﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class CostumeChanger : MonoBehaviour
{
    public Transform custume_steampunk;
    public Transform custume_blue;
    public Transform custume_astronaut;
    public Transform custume_santa;

    public Transform hair;

    public Texture tex_steampunk;
    public Texture tex_blues;
    public Texture tex_astronaut;
    public Texture tex_santa;

    public Color steampunk_hair_color;
    public Color blues_hair_color;
    public Color astronaut_hair_color;
    public Color santa_hair_color;

    Transform hero_body;
    Transform suit_collection;

    private void Start()
    {
       // hero_body = gameObject.GetComponent<Transform>().Find("body");
        hero_body = transform.Find("body"); // это короткая запись строчки выше
        suit_collection = transform.Find("SuitCollection");

        CustumeButton.OnSuit += ChangeSuit;
    }

    private void ChangeSuit(WhichSuit mWhichSuit)
    {
        NGUITools.SetActiveChildren(suit_collection.gameObject, false);

        if (mWhichSuit == WhichSuit.Steampunk)
        {
            custume_steampunk.gameObject.SetActive(true);
            hero_body.GetComponent<Renderer>().material.SetTexture("_MainTex", tex_steampunk);
            hair.GetComponent<Renderer>().material.SetColor("_Color", steampunk_hair_color);
        }

        if (mWhichSuit == WhichSuit.Blues)
        {
            custume_blue.gameObject.SetActive(true);
            hero_body.GetComponent<Renderer>().material.SetTexture("_MainTex", tex_blues);
            hair.GetComponent<Renderer>().material.SetColor("_Color", blues_hair_color);
        }

        if (mWhichSuit == WhichSuit.Santa)
        {
            custume_santa.gameObject.SetActive(true);
            hero_body.GetComponent<Renderer>().material.SetTexture("_MainTex", tex_santa);
            hair.GetComponent<Renderer>().material.SetColor("_Color", santa_hair_color);
        }

        if (mWhichSuit == WhichSuit.Astronaut)
        {
            custume_astronaut.gameObject.SetActive(true);
            hero_body.GetComponent<Renderer>().material.SetTexture("_MainTex", tex_astronaut);
            hair.GetComponent<Renderer>().material.SetColor("_Color", astronaut_hair_color);
        }
    }
}
