﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HeroMover : MonoBehaviour
{
    public float jerkLimiter; //рывок (ускорение) 
    public float angleTurnSpeed;
    public Animator heroAnimator;
    public ParticleSystem psystemIce;

    private void Start()
    {
        heroAnimator = GetComponent<Animator>();
    }

    private void OnDrag(Vector2 delta)
    {
        if (heroAnimator)
            heroAnimator.SetBool("walking", true);
        

        UIDragObject uiDragObject = GetComponent<UIDragObject>();
        //if (Mathf.Abs(delta.x) > jerkLimiter || Mathf.Abs(delta.y) > jerkLimiter)
        //{
        //    uiDragObject.dragMovement = new Vector3(0.1f, 0, 0.1f);
        //}
        //else
        //{
        //    uiDragObject.dragMovement = new Vector3(1, 0, 1);
        //}

        uiDragObject.dragMovement = Mathf.Abs(delta.x) > jerkLimiter || Mathf.Abs(delta.y) > jerkLimiter ? new Vector3(0.1f, 0, 0.1f) : new Vector3(1, 0, 1);

        if (psystemIce)
            psystemIce.Emit(1);

        Vector3 curPos = transform.localPosition;
        StartCoroutine(TimelyTurner(curPos));
    }

    public IEnumerator TimelyTurner(Vector3 pointFrom)
    {
        yield return new WaitForSeconds(0.05f);
        Vector3 pointTo = transform.localPosition;
        float angle = AngleCalculator(pointFrom, pointTo);       
        Vector3 heroRotFrom = transform.localEulerAngles;
        Vector3 heroRotTo = new Vector3(heroRotFrom.x, angle, heroRotFrom.z);

        float i = 0;
        while (i < 1)
        {
            transform.rotation = Quaternion.Slerp(Quaternion.Euler(heroRotFrom), Quaternion.Euler(heroRotTo), i);
            i += Time.deltaTime * angleTurnSpeed;      
            yield return null;
        }
    }

    private float AngleCalculator(Vector3 pointStart, Vector3 pointEnd)
    {
        float deltaX = pointEnd.x - pointStart.x;
        float deltaZ = pointEnd.z - pointStart.z;
        float angle = Mathf.Atan2(deltaX, deltaZ) * Mathf.Rad2Deg;

        return angle;
    }

    private void OnPress(bool isPressed)
    {
        if (!isPressed)
        {
            if (heroAnimator)
                heroAnimator.SetBool("walking", false);
        }       
    }
}
