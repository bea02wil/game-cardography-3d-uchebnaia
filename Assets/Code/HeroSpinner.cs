﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSpinner : MonoBehaviour
{
    public float turnSpeed;
    Quaternion prevRot;

    private void Start()
    {
        ViewsToggler.OnViewMode += StartSpin;
    }

    private void StartSpin(WhichButton mWhichButton)
    {
        if (mWhichButton == WhichButton.CharDes)
        {
            prevRot = transform.rotation;
            StartCoroutine(HeroSpinnerCoroutine());
        }
        else
        {
            StopAllCoroutines();
            transform.rotation = prevRot;
        }
    }

    private IEnumerator HeroSpinnerCoroutine()
    {
        while (true)
        {
            transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed);
            yield return null; //перейти на след фрейм в апдейте
        }       
    }

    void Update()
    {
        
    }
}
