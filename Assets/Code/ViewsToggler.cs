﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WhichButton
{
    CharDes,
    Rink
};

public class ViewsToggler : MonoBehaviour
{
    public WhichButton mWichButton;
    public delegate void ViewModeHandler(WhichButton someWhichButton);
    public static event ViewModeHandler OnViewMode;

    Vector3 pos;
    public float offsetForTweened;

    private void Start()
    {
        ViewsToggler.OnViewMode += ChangeViewMode;
        GetComponent<UIRect>().UpdateAnchors();
        pos = transform.localPosition;

        OnViewMode?.Invoke(WhichButton.Rink);
    }

    public void ChangeViewMode(WhichButton someWhichButton)
    {
        if (someWhichButton == mWichButton)
        {
            TweenPosition.Begin(gameObject, 0.3f, new Vector3(pos.x + offsetForTweened, pos.y, pos.z));          
        }
        else if (someWhichButton != mWichButton)
        {
            TweenPosition.Begin(gameObject, 0.3f, pos);         
        }
    }

    private void OnClick() //это метод NGUI
    {
        OnViewMode?.Invoke(mWichButton);
    }
    
}
